
import numpy as np

def ma_func(f,p1,p2,n,t=1):
  assert( n > 1 )
  ev = np.linspace(p1,p2,n)
  if t == 1:
    i = 0
    for v in ev[:-1]:
      i += f(v)
    i*=n/(p2-p1)
  elif t == 2:
    i = 0
    for v in ev[1:]:
      i += f(v)
    i*=n/(p2-p1)
  elif t == 3:
    i = 0
    for k, v in enumerate(ev[1:]):
      i += f(v)-f(ev[k])
    i*=n/(2*(p2-p1))

  return i

import math as m

print( "resultat 1 : ", ma_func(m.cos,0,2*m.pi,6) )
print( "resultat 2 : ", ma_func(m.cos,0,2*m.pi,6,2) )
print( "resultat 2b: ", ma_func(m.cos,0,2*m.pi,30,2) )
print( "resultat 3 : ", ma_func(m.cos,0,2*m.pi,6,3) )
for i in range(3):
  print( "resultat "+str(i+1)+"c: ", ma_func(m.cos,0,m.pi,6,i+1) )

